import { FormSubmissionPage } from './app.po';

describe('form-submission App', () => {
  let page: FormSubmissionPage;

  beforeEach(() => {
    page = new FormSubmissionPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
